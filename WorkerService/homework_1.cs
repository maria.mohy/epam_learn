using System;

namespace Homework_1
{
    class Program
    {
        public static int Task1(int a)
        {
            int result;
            if (a > 0)
            {
                result = a * a;
            }

            else if (a < 0)
            {
                result = Math.Abs(a);

            }
            else
            {
                result = 0;
            }

            return result;
        }
        public static int Task2(int a)
        {
        int result = 0;
            int hundreds = a / 100;
            int dozens = (a - hundreds*100) / 10;
            int units = a % 10;
            int result1 = Math.Max(hundreds, units);
            int maximum = Math.Max(result1, dozens);
            int result3 = Math.Min(hundreds, units);
            int minimum = Math.Min(result3, dozens);
            int mid = 0;
            if (maximum > hundreds)
            {
                mid = hundreds;
            }
            
            if (maximum >= dozens)
            {
                mid = dozens;
            }
            if (maximum > units)
            {
                mid = dozens;
            }
            result = maximum * 100+ mid * 10 + minimum;
            return result;
        }                             
    }
}