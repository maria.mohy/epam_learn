using System.Linq;

namespace Homework_2
{
    class Program
    {
        public static int Task1(int n)
        {
            string num = n.ToString();
            var oddNums = num.Where(x=> int.Parse(x.ToString())%2!=0);
            var oddsum = 0;
            foreach (char odd in oddNums)
            {
                oddsum += int.Parse(odd.ToString());
            }
            return oddsum;
        }
        public static int Task2(int n)
        {
            int remainder;
            int result = 0;
            while (n>0)
            { 
                remainder = n % 2;
                if (remainder ==1)
                {
                    result += 1;
                }
                n /= 2;
            }
            return result;
        }
        public static int Task3(int n)
        {
            int n1 = 0, n2 = 1, n3, i, sum=0;
            for (i = 2; i < n; ++i)
            {
                n3 = n1 + n2;
                n1 = n2;
                n2 = n3;
                sum += n3;
            }
            if (n <= 1)
            {
                sum = 0;
            }
            return sum;
        }
        
    }   
}